# pathFinder
My early implementation attempt of Priceless Path Exploration (BFS-like) and Bulk Route Creation Algoritm (PPE-BRC)

## Some backstory for any visitors
This project was intially started on May 30th, 2020, as a side project of mine to built something with Dart. It was like my very first week studying Dart and to whom it may concern, I'd like to apologize for the code quality that you'll find here. I don't have any idea as to whether this project would still run against the latest version of Dart and I have stopped using Dart since my last commit within the same year. There are 3 test files that you can use to test the project but let me remind you that it will require you to install `xdotool`, otherwise, you will need to run the project and do the graph input manually which is going to be somewhat lengthly.

## Project Status
I'd still be opening this project for comments and questions through the issue board. The project itself is in a stale state due to some issues (or bugs) left that I will explain below. I will leave the code as be and would start from scratch with a different programming language (TypeScript, and probably Kotlin (JVM)) as my thesis for this semester onwards.

## Techincal Overview of the Algorithm
The program would run into 2 different process below :

1. Route Exploration  
  In this phase, the program would exlpore the graph in a BFS way ignoring any price, cost, or weight. For every X visited node, add X to the visited node as long as X is not visited from a known Y adjacent node. The example would be the one below :

```txt
Graph :
a->c
b->c
```

On the example above, there'll be 2 stacks of C because C is visited twice from different adjacent node. Therefore, C will be given weight of N amount which in this case is 2, and will be used later for the 2nd phase.

2. Route Creation  
For every single stack of Node X, pop X out of the stack, and do a backtrack to it's previous most highest adjacent Node Y, and continue until it reach the starting Node Z. If there are only one X and Y node left in the stack, then it'll be used to create the very last route and then the program would declare a finish. After that, the array of nodes will be reversed and printed.

## Known Issues
* Duplicate Routes  
  I haven't properly investigated this yet till date but here is some ideas I can tell about the issue. The path exploration is working as expected, the route creation part does seem to have some odd behavior that I haven't been able to debug. The route produced are all valid, but there are a few routes that are duplicates and they are most likely produced near the end of the iteration. Let's say there are 7 routes produced, the last 4 are likely to be duplicates.

* Unoptimized Code  
  As of writing, nothing has been done since my last commit to optimise the code. Performance are very likely an issue due to how poorly it was written.

## License
Please have a look at the `LICENSE.txt` file for details. Be free to fork and cite this project as long as citation is reserved.

