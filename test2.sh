#!/bin/sh

# Launch
xdotool type 'dart app.dart'
xdotool key "Return"

# Node New York
xdotool type 'New York'
xdotool key "Return"
xdotool type 'Cleveland,St. Louis,Nashville'
xdotool key "Return"
xdotool type '400'
xdotool key "Return"
xdotool type '950'
xdotool key "Return"
xdotool type '800'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node Cleveland
xdotool type 'Cleveland'
xdotool key "Return"
xdotool type 'New York,Phoenix,Dallas'
xdotool key "Return"
xdotool type '400'
xdotool key "Return"
xdotool type '1800'
xdotool key "Return"
xdotool type '900'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node St. Louis
xdotool type 'St. Louis'
xdotool key "Return"
xdotool type 'New York,Phoenix,Dallas'
xdotool key "Return"
xdotool type '950'
xdotool key "Return"
xdotool type '1100'
xdotool key "Return"
xdotool type '600'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node Nashville
xdotool type 'Nashville'
xdotool key "Return"
xdotool type 'New York,Dallas,Salt Lake City'
xdotool key "Return"
xdotool type '800'
xdotool key "Return"
xdotool type '600'
xdotool key "Return"
xdotool type '1200'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node Phoenix
xdotool type 'Phoenix'
xdotool key "Return"
xdotool type 'Cleveland,St. Louis,Dallas,Los Angeles'
xdotool key "Return"
xdotool type '1800'
xdotool key "Return"
xdotool type '1100'
xdotool key "Return"
xdotool type '900'
xdotool key "Return"
xdotool type '400'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node Dallas
xdotool type 'Dallas'
xdotool key "Return"
xdotool type 'Cleveland,St. Louis,Nashville,Phoenix,Salt Lake City,Los Angeles'
xdotool key "Return"
xdotool type '900'
xdotool key "Return"
xdotool type '600'
xdotool key "Return"
xdotool type '600'
xdotool key "Return"
xdotool type '900'
xdotool key "Return"
xdotool type '1000'
xdotool key "Return"
xdotool type '1300'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node Salt Lake City
xdotool type 'Salt Lake City'
xdotool key "Return"
xdotool type 'Nashville,Dallas,Los Angeles'
xdotool key "Return"
xdotool type '1200'
xdotool key "Return"
xdotool type '1000'
xdotool key "Return"
xdotool type '600'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node Los Angeles
xdotool type 'Los Angeles'
xdotool key "Return"
xdotool type 'Phoenix,Dallas,Salt Lake City'
xdotool key "Return"
xdotool type '400'
xdotool key "Return"
xdotool type '1300'
xdotool key "Return"
xdotool type '600'
xdotool key "Return"
xdotool type 'no'
xdotool key "Return"