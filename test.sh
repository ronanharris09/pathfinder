#!/bin/sh

# Launch
xdotool type 'dart app.dart'
xdotool key "Return"

# Node A
xdotool type 'A'
xdotool key "Return"
xdotool type 'B,C'
xdotool key "Return"
xdotool type '3'
xdotool key "Return"
xdotool type '4'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node B
xdotool type 'B'
xdotool key "Return"
xdotool type 'A,C,D,E'
xdotool key "Return"
xdotool type '3'
xdotool key "Return"
xdotool type '3'
xdotool key "Return"
xdotool type '5'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node C
xdotool type 'C'
xdotool key "Return"
xdotool type 'A,B,E'
xdotool key "Return"
xdotool type '4'
xdotool key "Return"
xdotool type '3'
xdotool key "Return"
xdotool type '6'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node D
xdotool type 'D'
xdotool key "Return"
xdotool type 'B,E,F'
xdotool key "Return"
xdotool type '5'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '3'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node E
xdotool type 'E'
xdotool key "Return"
xdotool type 'B,C,D,F'
xdotool key "Return"
xdotool type '2'
xdotool key "Return"
xdotool type '6'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type 'yes'
xdotool key "Return"

# Node F
xdotool type 'F'
xdotool key "Return"
xdotool type 'D,E'
xdotool key "Return"
xdotool type '3'
xdotool key "Return"
xdotool type '1'
xdotool key "Return"
xdotool type 'no'
xdotool key "Return"