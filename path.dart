/*
  Copyright 2020 Ronan Harris
  Licensed under MIT
  twitter.com/Ronanharris4
*/

// Class to represent the path of a graph that holds the weight / distance
// of a path connected  to a node that includes the name of the connected node.
class Path {
  String connectedNode;
  int distance;

  Path({this.connectedNode = '', this.distance = -1});
}